export interface AppLinks {
  label:string;
  baseUrl: string;
  type: string;
}

export interface FooterLinks {
  label:string,
  links:AppLinks[]
}