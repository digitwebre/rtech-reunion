import { Logo } from "@/ui/design-system/logo/Logo"
import { Container } from "../container/Container"
import { Typography } from "@/ui/design-system/typography/typography"
import { Button } from "@/ui/design-system/button/button"
import Link from "next/link"
import { ActiveLink } from "./active-link"

interface Props{}

export const Navigation = ({}: Props) => {
  return (
    <div className="border-b-2 border-gray-400 ">
      <Container className="flex justify-between items-center gap-7 py-3">

        <Link href="/">
          <div className="flex items-center gap-1">
            <Logo size="large"/>
          </div>
        </Link>
        <div className="flex items-center gap-7">
          <Typography variant="caption3" theme="gray" component="div" className="flex items-center gap-7">
            <ActiveLink href="/reprogrammations">
              Reprogrammations
            </ActiveLink>
            <ActiveLink href="/mecanique">
              Mécanique
            </ActiveLink>
            <ActiveLink href="/shop">
              Shop
            </ActiveLink>
            <ActiveLink href="/codage-bmw">
              Codage BMW
            </ActiveLink>
            <ActiveLink href="/contact">
              Contact
            </ActiveLink>
            {/* <Link href="/design-system">Services</Link>
            <Link href="/portfolio">Portfolio</Link>
            <Link href="/boutique">Boutique</Link>
            <Link href="/blog">Blog</Link>
            <Link href="/contact">Contact</Link> */}
          </Typography>
          <div className="flex items-center gap-2">
            <Button size="small" variant="danger" iconTheme="secondary" >
              Connexion
            </Button>
            <Button size="small" variant="primary" iconTheme="secondary" >
              Rejoindre
            </Button>
          </div>
        </div>
      </Container>
    </div>
  )
}