'use client';

import clsx from "clsx";
import Link from "next/link";
import { usePathname } from 'next/navigation';
import { useMemo } from "react";

interface Props{
  href: string;
  children:string;
}

export const ActiveLink = ({href, children}: Props) => {
  const path = usePathname();
  // console.log(path);

  const isActive: boolean = useMemo(() => {
    return path === href
  }, [path])

  return(
    <Link href={href} className={clsx(isActive && "text-danger font-medium")}>{children}</Link>
  )
}
