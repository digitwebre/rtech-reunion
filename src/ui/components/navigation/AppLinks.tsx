import { AppLinks } from "@/types/AppLinks"

export const footerApplicationLinks:AppLinks [] =[
  {
    label: "Accueil",
    baseUrl: "/",
    type: "internal"
  },
  {
    label: "Reprogrammations",
    baseUrl: "/reprogrammations",
    type: "internal"
  },
  {
    label: "Mécanique",
    baseUrl: "/mecanique",
    type: "internal"
  },
  {
    label: "Codage BMW",
    baseUrl: "/codage-bmw",
    type: "internal"
  },
  {
    label: "Contact",
    baseUrl: "/contact",
    type: "internal"
  },
]
export const footerUsersLinks: AppLinks[] =[
  {
    label: "Mon espace",
    baseUrl: "/#",
    type: "internal"
  },
  {
    label: "Connexion",
    baseUrl: "/#",
    type: "internal"
  },
  {
    label: "Inscription",
    baseUrl: "/#",
    type: "internal"
  },
  {
    label: "Mot de passe oublié",
    baseUrl: "/#",
    type: "internal"
  },
]
export const footerInformationsLinks: AppLinks[] =[
  {
    label: "CGU",
    baseUrl: "/#",
    type: "internal"
  },
  {
    label: "Confidentialité",
    baseUrl: "/#",
    type: "internal"
  },
  {
    label: "A propos",
    baseUrl: "/#",
    type: "internal"
  },
  {
    label: "Contact",
    baseUrl: "/#",
    type: "internal"
  },
]
export const footerSocialLinks: AppLinks[] =[
  {
    label: "Facebook",
    baseUrl: "/#",
    type: "external"
  },
  {
    label: "TikTok",
    baseUrl: "/#",
    type: "external"
  },
  {
    label: "Instagram",
    baseUrl: "/#",
    type: "external"
  },
  {
    label: "Snapchat",
    baseUrl: "/#",
    type: "external"
  },
]

export const footerLinksMaster = [
  {
    label: "App",
    links: footerApplicationLinks
  },
  {
    label: "Utilisateurs",
    links: footerUsersLinks
  },
  {
    label: "Informations",
    links: footerInformationsLinks
  },
  {
    label: "Social Network",
    links: footerSocialLinks
  },
]