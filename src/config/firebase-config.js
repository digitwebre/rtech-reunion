// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyAc8n7W_UlWoHUNZYu0HT40bt6R5NqFUdM",
  authDomain: "rtechreunion-re.firebaseapp.com",
  projectId: "rtechreunion-re",
  storageBucket: "rtechreunion-re.appspot.com",
  messagingSenderId: "937068838307",
  appId: "1:937068838307:web:1dcc96f5fec251691dae16"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);