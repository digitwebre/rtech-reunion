import { Container } from "@/ui/components/container/Container";
import { Typography } from "@/ui/design-system/typography/typography";

export default function Reprogrammations(){
  return(
    <Container>
      <Typography variant="h1" theme="primary">
        Reprogrammez votre véhicule
      </Typography>
      <Typography variant="caption1" theme="gray">
        Reprogrammez votre véhicule
      </Typography>
    </Container>
  )
}