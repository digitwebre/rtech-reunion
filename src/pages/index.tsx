import { Container } from "@/ui/components/container/Container";
import { Footer } from "@/ui/components/navigation/Footer";
import { Navigation } from "@/ui/components/navigation/Navigation";
import { Seo } from "@/ui/components/seo/seo";
import { Button } from "@/ui/design-system/button/button";
import { Typography } from "@/ui/design-system/typography/typography";

export default function Home() {
  return (
    <>
      <Seo
        description="Rtech & C2 Motorsport"
      />
      <Navigation />
      <Footer />
    </>
  );
}
